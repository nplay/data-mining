module.exports = function (api) {
  api.cache(true);

  const presets = [
    [
    '@babel/preset-env'
    ]
   ];

  const ignore = [/node_modules\/(?!core)/];

  const plugins = [  ];

  return {
    presets,
    plugins,
    ignore
  };
}

