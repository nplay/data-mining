import sys
import nltk
from math import sqrt
import math
import json
#import numpy as np
from string import punctuation
import pandas as pd
from sklearn.feature_extraction.text  import TfidfVectorizer

#data = json.loads(sys.argv[1])

stopwords = list(punctuation)

def normalizar(data):
  data = data.lower()
  data = data.translate({ord(c): " " for c in punctuation})

  data = nltk.probability.FreqDist(nltk.word_tokenize(data))
  tokenizada = list(data.keys())
  tokenizada = [x for x in tokenizada if not x.isdigit()]

  semstopWords = [palavra for palavra in list(tokenizada) if palavra not in stopwords]
  semstopWords = ' '.join(semstopWords)

  stemmer = nltk.stem.RSLPStemmer()
  dataStemmer = stemmer.stem(semstopWords)
  return dataStemmer

path = sys.argv[1]
#path = '/tmp/e307a710-17ae-11eb-8639-bd3b5b338fdb.json'
dt = pd.read_json(path)
dt = dt[dt.title != '']

#dt = dt[:4000].reset_index(drop=True)
#dt = dt.sample(frac=1).reset_index(drop=True)

#Normalização
for idx, row in dt.iterrows():
  dt.at[idx, 'title'] = normalizar(row['title'])

#Frequencia inversa (Term frequency - inverse document frequency)
vectorizer = TfidfVectorizer(stop_words=nltk.corpus.stopwords.words('portuguese'))
matrix = vectorizer.fit_transform(dt['title'])
matrixArray = matrix.toarray()
matrixLength = len(matrix.toarray())

from sklearn.metrics.pairwise import linear_kernel

#print('Será um vetor de coluna ex: 1x256, em que cada coluna será uma pontuação de similaridade com cada item\n')

dictItens = []

for idx in range(len(matrixArray)):
  matrixItem = matrixArray[idx]
  shape = matrixItem.reshape(1,-1)
  cosine_sim = linear_kernel(shape, matrix)

  sim_scores = list(enumerate(cosine_sim[0]))
  sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)

  indices = [i[0] for i in sim_scores[:50] if i[0] != idx]

  dictItens.append({
    "id": dt.loc[idx].id,
    "ids": list(dt['id'].iloc[indices])
  })

import json

jsonItens = json.dumps(dictItens)

print(jsonItens)

#with open('result.json', 'w') as json_file:
#  json.dump(dictItens, json_file)