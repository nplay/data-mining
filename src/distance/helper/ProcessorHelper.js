
import {PythonShell} from 'python-shell';
const fs = require('fs');
import { v1 as uuidv1 } from 'uuid';

class ProcessorHelper
{
    constructor(body)
    {
        this._body = body
    }

    async get()
    {
        return await this._proccess(this._body)
    }

    _proccess(data)
    {
        return new Promise((resolve, reject) => {

            let filePath = `/tmp/${uuidv1()}.json`
            fs.writeFileSync(filePath, JSON.stringify(data));

            let options = {
                args: [
                    filePath
                ]
              };

            let path = __dirname + '/../index.py'

            PythonShell.run(path, options, function (err, result)
            {
                if (err)
                {
                    fs.unlinkSync(filePath);
                    return reject(err.toString())
                }

                if(!Array.isArray(result)){
                    fs.unlinkSync(filePath);
                    return reject(`Distance result is not valid`)
                }

                let response = JSON.parse(result)

                fs.unlinkSync(filePath);

                resolve(response)
            });
        })
    }
}

export default ProcessorHelper