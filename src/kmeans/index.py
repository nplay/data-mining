import sys
import nltk
from math import sqrt
import math
import json
#import numpy as np
from string import punctuation
import pandas as pd
from sklearn.cluster import MiniBatchKMeans, KMeans
from sklearn.feature_extraction.text  import TfidfVectorizer

#data = json.loads(sys.argv[1])

def normalizar(data):
  data = data.lower()
  data = data.translate({ord(c): " " for c in punctuation})

  data = nltk.probability.FreqDist(nltk.word_tokenize(data))
  tokenizada = list(data.keys())

  stopwords = nltk.corpus.stopwords.words('portuguese') + list(punctuation)

  semstopWords = [palavra for palavra in list(tokenizada) if palavra not in stopwords]
  semstopWords = ' '.join(semstopWords)

  stemmer = nltk.stem.RSLPStemmer()
  dataStemmer = stemmer.stem(semstopWords)
  return dataStemmer

def elbow(data, range):
  wcss =[]

  for i in range:
    #kmeans = KMeans(n_clusters = i, init = 'random')
    kmeans = MiniBatchKMeans(init ='k-means++', n_clusters = i, batch_size = 10)
    kmeans.fit(data)
    wcss.append(kmeans.inertia_)
  
  return wcss

def optimal_number_of_clusters(wcss):
    x1, y1 = 2, wcss[0]
    x2, y2 = 20, wcss[len(wcss)-1]

    distances = []
    for i in range(len(wcss)):
        x0 = i+2
        y0 = wcss[i]
        numerator = abs((y2-y1)*x0 - (x2-x1)*y0 + x2*y1 - y2*x1)
        denominator = sqrt((y2 - y1)**2 + (x2 - x1)**2)
        distances.append(numerator/denominator)
    
    #return distances.index(max(distances)) + 2
    return distances.index(max(distances)) + 2

path = sys.argv[1]
#path = '/tmp/903fc730-f43e-11ea-addc-af27b310b25e.json'
dt = pd.read_json(path)
dt = dt[dt.title != '']

#Normalização
for idx, row in dt.iterrows():
  dt.at[idx, 'title'] = normalizar(row['title'])

#Frequencia inversa (Term frequency - inverse document frequency)
vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(dt['title'])

tf_idf = pd.DataFrame(data = X.toarray(), columns=vectorizer.get_feature_names())

#KMEANS
sizeDt = len(tf_idf.index)

minItens = 10 if sizeDt > 10 else sizeDt
maxClusters = sizeDt / minItens
maxClusters = math.floor(maxClusters) if math.floor(maxClusters) > 1 else 2

maxClusters = 20 if maxClusters > 20 else maxClusters

_range = range(2, maxClusters+1)
wcss = elbow(tf_idf, _range)
clusters = optimal_number_of_clusters(wcss)

#kmeans = KMeans(n_clusters = clusters, init = 'random')
kmeans = MiniBatchKMeans(init ='k-means++', n_clusters = clusters, batch_size = 10)
kmeans.fit_predict(tf_idf)

#Exibindo dataframe com os grupos
dt['group'] = kmeans.labels_

dt = dt.drop('title', 1)

jsonData = dt.to_json(None, orient='records')
print(jsonData)