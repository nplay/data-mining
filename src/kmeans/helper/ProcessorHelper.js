
import {PythonShell} from 'python-shell';
const fs = require('fs');
import { v1 as uuidv1 } from 'uuid';

class ProcessorHelper
{
    constructor(body)
    {
        this._body = body
    }

    async get()
    {
        return await this._processKMeans(this._body)
    }

    _processKMeans(data)
    {
        return new Promise((resolve, reject) => {
            if(data.length <= 1) //K-means minimium 2 clusters
                return resolve([]);

            let filePath = `/tmp/${uuidv1()}.json`
            fs.writeFileSync(filePath, JSON.stringify(data));

            let options = {
                args: [
                    filePath
                ]
              };

            let path = __dirname + '/../index.py'

            PythonShell.run(path, options, function (err, result)
            {
                if (err)
                {
                    fs.unlinkSync(filePath);
                    return reject(err.toString())
                }

                if(!Array.isArray(result)){
                    fs.unlinkSync(filePath);
                    return reject(`K-means result is not valid`)
                }

                result = result[0]
                let kmeansResponse = JSON.parse(result)

                fs.unlinkSync(filePath);

                //kmeansResponse
                resolve(kmeansResponse)
            });
        })
    }
}

export default ProcessorHelper