
import events2 from 'event2'
import { BuyToBuyRequest } from './listener/buyToBuyRequest'
import { ViewToViewRequest } from './listener/viewToViewRequest'

export default {
    name: "data-mining.association",
    version: 1,
    mixins: [events2],
    actions: {
    },
    events2: [
        {
            event:'v1.analytics.buyToBuyRequest',
            listeners: {
                retrive: {
                    handle: new BuyToBuyRequest().execute,
                    createDeadLetter: false
                }
            }
        },
        {
            event:'v1.storage.viewToViewRequest',
            listeners: {
                retrive: {
                    handle: new ViewToViewRequest().execute,
                    createDeadLetter: false
                }
            }
        }
    ]
}