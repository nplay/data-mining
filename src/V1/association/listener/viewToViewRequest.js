import ProcessorHelper from "../../../apriori/helper/ProcessorHelper"

class ViewToViewRequest
{
    async execute(content, message, ok, fail){

        let body = null
        let identifier = null

        body = content.params
        identifier = body.identifier
        let itens = body.itens

        if(!identifier)
            throw new Error(`Identifier not defined`)
        else if(!itens)
            throw new Error(`No itens specified`)

        try {

            let processorHelper = new ProcessorHelper()
            let output = await processorHelper.get(itens)

            this.emit2({}, 'v1.data-mining.viewToViewRequest', {
                identifier: identifier,
                itens: output
            })

            ok(message)
        } catch (error) {
            this.emit2({}, 'v1.any.fatalError', {
                identifier: identifier,
                context: 'Listener viewToViewRequest data-mining.association',
                subject: `${identifier} solicitação falhou`,
                error: error.message,
                attachments: [
                    {
                        filename: 'data.log',
                        content: JSON.stringify({
                            identifier: identifier,
                            data: body
                        }),
                        contentType: 'text/plain'
                    },
                    {
                        filename: 'stackTrace.log',
                        content: error.stack,
                        contentType: 'text/plain'
                    }
                ]
            })

            fail(message)
        }
    }
}

export {
    ViewToViewRequest
}