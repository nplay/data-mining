
import SimilarService from './similar/service'
import AssociationService from './association/service'

export default async (broker) => {
    broker.createService(SimilarService)
    broker.createService(AssociationService)
}