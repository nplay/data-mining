
import events2 from 'event2'
import { Retrive } from './listener/retrive'

export default {
    name: "similar",
    version: 1,
    mixins: [events2],
    actions: {
    },
    events2: [
        {
            event:'v1.storage.similarRequest',
            listeners: {
                retrive: {
                    handle: new Retrive().execute,
                    createDeadLetter: false
                }
            }
        }
    ]
}