import ProcessorHelper from "../../../distance/helper/ProcessorHelper"

class Retrive
{
    async execute(content, message, ok, fail){

        let body = null
        let identifier = null
        let itens = null
        body = content.params
        identifier = body.identifier
        itens = body.itens

        if(!identifier)
            throw new Error(`Identifier not defined`)
        else if(!itens)
            throw new Error(`Itens not defined`)

        try {
            let processor = new ProcessorHelper(itens)
            let body = await processor.get()

            this.emit2({}, 'v1.ipapi.similarResult', {
                identifier: identifier,
                itens: body
            })

            ok(message)
        } catch (error) {

            this.emit2({}, 'v1.ipapi.similarResultFail', {
                identifier: identifier,
                context: 'Evento "v1.ipapi.similarResultFail"',
                error: error.toString(),
                attachments: [
                    {
                        filename: 'data.log',
                        content: JSON.stringify({identifier: identifier, itens: itens}),
                        contentType: 'text/plain'
                    },
                    {
                        filename: 'stackTrace.log',
                        content: error.stack,
                        contentType: 'text/plain'
                    }
                ]
            })
            

            fail(message)
        }
    }
}

export {
    Retrive
}