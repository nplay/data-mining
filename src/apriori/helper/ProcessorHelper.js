
import {PythonShell} from 'python-shell';

class ProcessorHelper
{
    /**
     * 
     * @param {Array} itens 
     */
    get(itens)
    {
        return new Promise((resolve, reject) => {
            let path = __dirname + '../../index.py'
    
            if(itens.length == 0)
                return resolve([])

            let options = {
                args: [JSON.stringify(itens)]
            };
    
            PythonShell.run(path, options, function (err, result) {
                if (err)
                    return reject(err)
    
                let data = JSON.parse(result)
                resolve(data)
            });
        })
    }
}

export default ProcessorHelper