try:
    import json
    import sys
    
    from apyori import apriori
        
    data = json.loads(sys.argv[1])
    #data = json.loads('[["nike1-verde"],["hurley123","nike1-verde"]]')
    #data = json.loads('[["hurley123","test","nike1-verde"]]')
    #data = json.loads('[["camisa-varias-cores-e-tamanho0001","havaianas-7754","hurley123"], ["camisa-varias-cores-e-tamanho0001", "bla2"]]')
    
    association_rules = apriori(data, min_support=0.001, min_confidence=0.5, min_lift=1, min_length=2)
    association_results = list(association_rules)
        
    output = []
    curIdx = 0
    
    for result in association_results:
        total = len(result.items)
        
        if total == 1 :
            continue
    
        ordernedItems = []
        
        itens = list(result.items)
        total = len(itens)
        
        if total == 1 :
            continue

        fromItem = itens[0]
        itens.pop(0)
        
        x = {
            "from": fromItem,
            "to": itens,
            "support": str(result.support),
            "confidence": str(result[2][0][2]),
            "lift": str(result[2][0][3])
        }
        
        output.append(x)
        
    print(json.dumps(output))
       
except Exception as e:
    print(e)